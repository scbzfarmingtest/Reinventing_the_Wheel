# Starting A New Prooject  
I do **NOT** quite sure if I can keep this project for a long time, since I have enormous stuff to learning and all before projects killed because of my poor endurance.  
WHATSOEVER, I hope at least I could learn something, and dream it could last till the very end.

# Journal
## 2021
### Aug
+ **Aug.12**
	Continue this project, with complete changes.  
	+ Re-arrange objects. Starting from easier *C Programmes*  
	+ Decide to follow repo
	+ Start [sub-project 001]()
### Jan
+ **Jan.24:**  
    Start the project, set up a repo and create a readme.md.  
    Decide to start [sub-project 001](https://github.com/scbzfarmingtest/Reinventing_the_Wheel/001_Build_a_database).
+ **Jan.26:** 

# References and  ACKNOWLEDGMENTS  
## Overall 
+ A repo link many open-source project. Many of 'wheels' are learning from this project: <br/>[Cgames](https://github.com/yh1094632455/Cgames) <br/>[danistefanovic's repo: build-your-own-x](https://github.com/danistefanovic/build-your-own-x)   

## Specificify refering Project  
**NB: References and *ACKNOWLEDGMENTS* for specific projects will list again in MD files of corresponding repo. Here is the overview.

### 1.

### ex-1. [Build a database](https://github.com/scbzfarmingtest/Reinventing_the_Wheel/tree/master/001_Build_a_database): [Tutorial Page](https://cstack.github.io/db_tutorial/) and [Repo Page](https://github.com/cstack/db_tutorial)
